#!/usr/bin/env python
# Argomenti da linea di comando
#  - File dati su cui ottimizzare

import sys
import numpy as np
import matplotlib.pyplot as plt
from cmath import rect

import utils

plt.style.use("style.yaml")

np_rect = np.vectorize(rect)

E, N0, N1, N2 = np.loadtxt(sys.argv[1], unpack=True, delimiter=',')
params = utils.get_parameters(sys.argv[1])
L = params['L']
M = abs(np_rect(N0 / L**3, 0) + np_rect(N1 / L**3, 2*np.pi / 3) + np_rect(N2 / L**3, 4*np.pi / 3))

BLOCK_SIZE_MAX = int(len(E) / 2)
print(len(E), BLOCK_SIZE_MAX)

fig = utils.block_optimize(M, BLOCK_SIZE_MAX, return_fig=True)
ax = fig.axes[0]
ax.set_xlabel("Block size")
ax.set_ylabel(r"$\sigma_m$")
ax.set_xscale("log")
ax.set_yscale("log")
ax.axvline(4000, linestyle='--', color="grey")
plt.figure(fig)
ax.set_ylim(0.0005, None)
plt.savefig("./imgs/blocking_m.png")
plt.close()

fig = utils.block_optimize(E, BLOCK_SIZE_MAX, return_fig=True)
ax = fig.axes[0]
ax.set_ylim(bottom=0.001, top=None)
ax.set_xlabel("Block size")
ax.set_ylabel(r"$\sigma_u$")
ax.set_xscale("log")
ax.set_yscale("log")
ax.axvline(4000, linestyle='--', color="grey")
plt.figure(fig)
ax.set_ylim(10, 3000)
plt.savefig("./imgs/blocking_e.png")
plt.close()