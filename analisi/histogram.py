#!/usr/bin/env python
# Arguments:
#  - Path of file containing pickled histogram data

import numpy as np
import matplotlib.pyplot as plt
from matplotlib import rcParams
from mpl_toolkits.mplot3d import Axes3D
import pickle
from scipy.optimize import curve_fit, OptimizeWarning
import warnings
from uncertainties import ufloat, correlated_values
import argparse

parser = argparse.ArgumentParser(description='Process histogram data.')
parser.add_argument('infile', metavar='file', type=str, nargs=1,
                    help='File containing pickled data')

# Graphical options
parser.add_argument('--plot-hists',
                    action='store_true',
                    help='Plot all histograms')
parser.add_argument('--stack3D',
                    action='store_true',
                    help='Plot histograms as stacked 3D slices')
parser.add_argument('--plot-trisurf',
                    action='store_true',
                    help='Add trisurface to 3D plot')
parser.add_argument('--log-plot',
                    action='store_true',
                    help='Plot in log scale')
parser.add_argument('--zoom-style',
                    action='store_true',
                    help='Style switch for betaC plots')
parser.add_argument('--tex-plots',
                    action='store_true',
                    help='Use LaTeX for plots')

# Processing options
parser.add_argument('--no-fit-gaussians',
                    action='store_false',
                    help='Do not fit gaussians')
parser.add_argument('--fit-minima',
                    action='store_true',
                    help='Fit minima values with power law')
parser.add_argument('--no-equal-peak-reweight',
                    action='store_false',
                    help='Do not do peak reweighting')
parser.add_argument('--no-print-table',
                    action='store_false',
                    help='Do not print table')
parser.add_argument('--fit-fss-gauss',
                    action='store_true',
                    help='Fit power law to results of gaussian fit')

args = parser.parse_args()

# Plot options
plotHists = args.plot_hists
stack3D = args.stack3D
plotTrisurf = args.plot_trisurf
logPlot = args.log_plot
ZoomStyle = args.zoom_style

# Analysis options
fitGaussians = args.no_fit_gaussians
fitMinima = args.fit_minima
equalPeakReweigth = args.no_equal_peak_reweight
printTable = args.no_print_table
fitFssGauss = args.fit_fss_gauss

if not plotHists:
    logPlot = False
    stack3D = False
if not stack3D:
    plotTrisurf = False
if not (fitGaussians and equalPeakReweigth):
    printTable = False
if not fitGaussians:
    fitFssGauss = False

# Matplotlib plot style
plt.style.use("style.yaml")
if args.tex_plots:
    rcParams["text.usetex"] = True

# Fit functions and setup
warnings.simplefilter("error", OptimizeWarning)

def bigaussian(x, A1, A2, mu1, mu2, sigma1, sigma2):
    return A1 * np.exp(-(x - mu1) ** 2 / (2 * sigma1 ** 2)) \
         + A2 * np.exp(-(x - mu2) ** 2 / (2 * sigma2 ** 2))

def gaussian(x, A, mu, sigma):
    return A * np.exp(-(x - mu) ** 2 / (2 * sigma ** 2))

# Main
if __name__ == "__main__":

    # Data loading
    with open(args.infile[0], 'rb') as f:
        data = pickle.load(f)

    if plotHists:
        figHist = plt.figure()

    # Array initialization
    if fitGaussians:
        # Parametri di fit
        mu1s = []
        mu2s = []
        xi1s = []
        xi2s = []
        C1s = []
        C2s = []
        chisqs = []

        # Quantità derivate
        e1s = []
        e2s = []
        s1s = []
        s2s = []

        # Grafici
        Lgauss = []
        betagauss = []
        betaCgauss = []

    if fitMinima:
        Lm = []
        betam = []
        minm = []
        maxm = []

    if equalPeakReweigth:
        Lpr = []
        betapr = []
        betaPpr = []

    Ls = np.unique(data['L'])
    for L in Ls:
        if L in [8,12,16,44]:
            pass

        if stack3D:
            fig = plt.figure()
            ax = fig.add_subplot(111, projection='3d')
            ax.set_title(f'{L}')
            ax.set_xlabel('E')
            ax.set_ylabel('beta')
            ax.set_zlabel('P(E)')

            if plotTrisurf:
                X = np.array([])
                Y = np.array([])
                Z = np.array([])

        dataL = data[np.where(data['L'] == L)]
        for _, beta, bins, hist in dataL:
            if (beta < 0.5504) or (beta > 0.5507):
                continue
                # pass
            print(f"L: {L}, beta: {beta}\n")

            ### Processing
            if True:
                # Bound delle due "metà"
                boundL = -1.68
                boundU = -1.63

                if fitGaussians:
                    peakval0 = 0.05
                    params0 = [
                        peakval0, peakval0,
                        *[-1.59, -1.78],
                        *[0.02, 0.07]]
                    try:
                        maskL = (bins < boundL)
                        maskU = (bins > boundU)
                        maskBoth = np.logical_or(maskL, maskU)
                        mask2 = (bins < -1.71) | (bins > -1.62)
                        poptL, pcovL = curve_fit(gaussian, bins[maskL], hist[maskL], p0 = params0[1::2])
                        poptU, pcovU = curve_fit(gaussian, bins[maskU], hist[maskU], p0 = params0[0::2])
                        popt = np.array([bins for z in zip(poptL, poptU) for bins in z])
                        # popt = np.array(params0)
                    except OptimizeWarning:
                        print("Il fit non fitta")
                    else:
                        idx = np.argsort(popt[2:4])
                        C1, C2 = popt[0 + idx]
                        mus = mu1, mu2 = popt[2 + idx]
                        xis = xi1, xi2 = popt[4 + idx]

                        s1, s2 = xis * (L ** (3/2))
                        e1, e2 = mus + beta * xis**2 * L**3
                        alpha = 2 * (mu1 - mu2) / ((xi1**2 - xi2**2) * L**3) + 2 * beta
                        gamma = 2 / ((xi1**2 - xi2**2) * alpha**2 * L**6)\
                                * (np.log((C1 * xi1) / (C2 * xi2)) + beta * L**3 * (mu1 - mu2)) \
                                + beta**2 / alpha**2
                        betaC = alpha * (1 + np.sqrt(1 - 4 * gamma)) / 2

                        print(f"mu1:\t{mu1:+1.5f},\tmu2:\t{mu2:+1.5f}")
                        print(f"xi1:\t{xi1:+1.5f},\txi2: \t{xi2:+1.5f}")
                        print(f"s1:\t{s1:+1.5f},\ts2:\t{s2:+1.5f}")
                        print(f"e1:\t{e1:+1.5f},\te2:\t{e2:+1.5f}")
                        print(f"alpha:\t{alpha:.5f},\tgamma:\t{gamma:.5f}")
                        print(f"betaC:\t{betaC}")
                        print("")

                        C1s.append(C1)
                        C2s.append(C2)
                        mu1s.append(mu1)
                        mu2s.append(mu2)
                        xi1s.append(xi1)
                        xi2s.append(xi2)
                        e1s.append(e1)
                        e2s.append(e2)
                        s1s.append(s1)
                        s2s.append(s2)
                        Lgauss.append(L)
                        betagauss.append(beta)
                        betaCgauss.append(betaC)

                if fitMinima:
                    maxL_idx = np.argmax([count if val < boundL else 0 for (val, count) in zip(bins,hist)])
                    maxU_idx = np.argmax([count if val > boundU else 0 for (val, count) in zip(bins,hist)])
                    max_avg = (hist[maxL_idx] + hist[maxU_idx]) / 2
                    min_val = np.min(hist[maxL_idx:maxU_idx])
                    Lm.append(L)
                    betam.append(beta)
                    minm.append(min_val)
                    maxm.append(max_avg)

                if equalPeakReweigth:
                    delta = 0.01
                    e_avg = np.sum(bins * hist)

                    histLog = np.log(hist[hist > 0])
                    binsLog = bins[hist > 0]

                    # Initial reweight
                    Dmax_cur = np.max(histLog[binsLog > boundU])\
                               - np.max(histLog[binsLog < boundL])
                    delta_beta = np.sign(Dmax_cur) * 8 / (np.max(binsLog - e_avg) * L**3)
                    beta_new = beta + delta_beta
                    reweighted = histLog - delta_beta * L**3 * (binsLog - e_avg)
                    Dmax_new = np.max(reweighted[binsLog > boundU])\
                               - np.max(reweighted[binsLog < boundL])

                    if (Dmax_cur * Dmax_new < 0):
                        # Ordina i beta e i loro associati Dmax
                        betaL, betaU = sorted([beta, beta_new])
                        DmaxL, DmaxU = np.array([Dmax_cur, Dmax_new])[np.argsort([beta, beta_new])]

                        # Bisezione
                        while (np.abs(DmaxU) > delta or np.abs(DmaxL) > delta):
                            beta_new = (betaL + betaU) / 2
                            reweighted = histLog - (beta_new - beta) * L**3 * (binsLog - e_avg)
                            Dmax_new = np.max(reweighted[binsLog > boundU])\
                                       - np.max(reweighted[binsLog < boundL])

                            if (Dmax_new * DmaxU > 0):
                                betaU = beta_new
                                DmaxU = Dmax_new
                            else:
                                betaL = beta_new
                                DmaxL = Dmax_new

                        beta_P = (betaU + betaL) / 2
                        # reweighted = histLog - (beta_P - beta) * L**3 * (binsLog - e_avg)
                        # print(beta_P)
                        # plt.step(binsLog, reweighted, color='blue', where='mid')
                        # plt.show()
                        Lpr.append(L)
                        betapr.append(beta)
                        betaPpr.append(beta_P)
                        print(f"{beta_P=}")

                    else:
                        print("Failed to start bisection")

                print("   ---   ")


            ### Grafici
            if plotHists:
                if stack3D:
                    ax.step(bins, hist, zs=beta, zdir='y', color='blue', alpha=0.7)

                    if plotTrisurf:
                        X = np.append(X, bins)
                        Y = np.append(Y, np.full(len(bins), beta))
                        Z = np.append(Z, hist)
                else:
                    # The figure object needs to be common, else we get a memory leak
                    # Because matplotlib sucks
                    ax = figHist.add_subplot(1,1,1)
                    xlims = [-2,-1.5]
                    maskLog = hist > 0
                    e_avg = np.sum(bins * hist)

                    if logPlot:
                        ax.step(bins[maskLog], np.log(hist[maskLog]), color='blue', where='mid')
                    else:
                        ax.step(bins, hist, color='blue', where='mid')

                    Xs = np.linspace(*xlims, 100)

                    if fitGaussians:
                        ax.plot(Xs, bigaussian(Xs, *popt), color='orange')
                        # ax.plot(Xs, bigaussian(Xs, *popt) * np.exp(beta * Xs), color='green')
                        # ax.plot(bins[mask2], hist[mask2], linestyle='', marker=',', color='red')

                    if fitMinima:
                        ax.plot(Xs, np.full_like(Xs, min_val))

                    if equalPeakReweigth and logPlot:
                        ax.step(bins[maskLog], reweighted, color='red', where='mid')

                    ax.set_xlim(xlims)
                    if not logPlot:
                        ax.set_ylim(0,0.2)

                    # plt.show()
                    figHist.suptitle(f"$\\beta: {beta}$")
                    ax.set_xlabel(r"$E$")
                    ax.set_ylabel(r"$P(E)$")
                    figHist.savefig(f"./imgs/raw/L_{L}_beta_{beta:.7f}.png")
                    figHist.clear()
                    # print(plt.get_fignums()) # Per verificare quante figure esistono

    if plotTrisurf:
        ax.plot_trisurf(X, Y, Z, antialiased=True, alpha=0.7)

    if plotHists:
        plt.close(figHist)

    if fitGaussians:
        # Conversioni ad array numpy
        C1s     = np.array(C1s)
        C2s     = np.array(C2s)
        mu1s    = np.array(mu1s)
        mu2s    = np.array(mu2s)
        xi1s    = np.array(xi1s)
        xi2s    = np.array(xi2s)
        e1s     = np.array(e1s)
        e2s     = np.array(e2s)
        s1s     = np.array(s1s)
        s2s     = np.array(s2s)
        betagauss    = np.array(betagauss)
        Lgauss       = np.array(Lgauss)
        betaCgauss   = np.array(betaCgauss)

        ### Media e deviazione standard a L fisso
        LgaussU = np.unique(Lgauss)
        betaCgaussN = np.empty(len(LgaussU))
        betaCgaussS = np.empty(len(LgaussU))

        for i, L in enumerate(LgaussU):
            betaCgaussN[i] = np.average(betaCgauss[Lgauss == L])
            betaCgaussS[i] = np.std(betaCgauss[Lgauss == L], ddof=1)

        ### Fit FSS
        if fitFssGauss:
            gauss_fss = lambda L, c, a, alpha: c + a / (L**alpha)
            gauss_fss = lambda L, c, a: c + a / (L**3)
            p0 = [np.max(betaCgaussN)
                 ,(np.min(betaCgaussN) - np.max(betaCgaussN)) * (np.min(LgaussU))**3]
                #  ,3]
            popt, pcov = curve_fit(gauss_fss, LgaussU, betaCgaussN, sigma=betaCgaussS, p0=p0)
            # (betaCgaussFSS, a, alpha) = correlated_values(popt, pcov)
            (betaCgaussFSS, a) = correlated_values(popt, pcov)
            alpha = ufloat(3,0)
            print(f"\\beta_{{c}}\\ap{{FSS}} = {betaCgaussFSS:.2uL} \qquad a = {a:.2uL} \qquad \\alpha = {alpha:.2uL}")

        ### Grafici
        fig, ax = plt.subplots(1,1)

        # betaCgauss in funzione di L, con linee a beta costante e barre d'errore
        if ZoomStyle:
            errorStyle = {'marker':'x', 'markersize':5, 'capsize':3, 'elinewidth':0.5}
            plotStyle = {'marker':'.', 'markersize':3}
        else:
            errorStyle = {'markersize': 0, 'capsize':3, 'elinewidth':1.5}
            plotStyle = {'markersize': 0}
        ax.errorbar(LgaussU, betaCgaussN, yerr = betaCgaussS,
                    linestyle='', color='k', **errorStyle)
        ax.plot(Lgauss, betaCgauss, label=f"{beta}", linestyle='', color='r', **plotStyle)

        if fitFssGauss:
            Xs = np.linspace(np.min(LgaussU), np.max(LgaussU), 1000)
            plt.plot(Xs, gauss_fss(Xs, *popt))

        ax.set_xlabel(r"$L$")
        ax.set_ylabel(r"$\beta_{c,g}$")
        fig.savefig("./imgs/betaCgauss_L.png")
        ax.clear()

        # betaCgauss in funzione di beta, con linee ad L costante
        for L in np.unique(Lgauss):
            maskPlt = Lgauss == L
            ax.plot(betagauss[maskPlt], betaCgauss[maskPlt], marker='.', label=f"{L}")
        ax.set_xlabel(r"$\beta$")
        ax.set_ylabel(r"$\beta_{c,g}$")
        plt.savefig("./imgs/betaCgauss_beta.png")
        plt.clf()

        # Due dati in funzione di beta, con linee a L costante, e viceversa (2 grafici 2D)
        # for (dat1, dat2, title) in [(e1s, e2s, "e"), (xi1s, xi2s, "xi"), (s1s, s2s, "s")]:
        #     fig, ax = plt.subplots(2,2)
        #     for L in np.unique(Lgauss):
        #         maskPlt = Lgauss == L
        #         ax[0,0].plot(betagauss[maskPlt], dat1[maskPlt], linestyle='-', marker='.', label=f"{L}")
        #         ax[0,1].plot(betagauss[maskPlt], dat2[maskPlt], linestyle='-', marker='.', label=f"{L}")
        #     ax[0,0].set_xlabel(r"$\beta$")
        #     ax[0,1].set_xlabel(r"$\beta$")
        #     for beta in np.unique(betagauss):
        #         maskPlt = betagauss == beta
        #         ax[1,0].plot(Lgauss[maskPlt], dat1[maskPlt], marker='.', label=f"{beta}")
        #         ax[1,1].plot(Lgauss[maskPlt], dat2[maskPlt], marker='.', label=f"{beta}")
        #     ax[1,0].set_xlabel(r"$L$")
        #     ax[1,1].set_xlabel(r"$L$")
        #     fig.suptitle(title)
        #     # for x in ax.flatten():
        #     #     ax.legend()
        #     plt.show()

    if fitMinima:
        Lm = np.array(Lm)
        betam = np.array(betam)
        minm = np.array(minm)
        maxm = np.array(maxm)
        for beta in np.unique(betam):
            # Dati
            maskBeta = betam == beta
            L = Lm[maskBeta]
            mins = minm[maskBeta]
            maxs = maxm[maskBeta]

            # Fit
            xs = np.log(L)
            ys = np.log(maxs - mins)
            retta = lambda x, a, c: a * x + c
            popt, pcov = curve_fit(retta, xs, ys, p0=[-2, 0])
            print(f"beta: {beta}, esponente: {popt[0]}")

            # Grafico
            plt.plot(xs, ys, linestyle='', marker='.')
            Xs = np.linspace(np.min(xs), np.max(xs), 100)
            plt.plot(Xs, retta(Xs, *popt))
            plt.title(f"$\\beta: {beta}$, esponente: ${popt[0]}$")
            plt.xlabel("$\\ln L$")
            plt.ylabel("$\\ln(\\max(P(E)) - \\min(P(E)))$")
            plt.savefig(f"./imgs/fit_minima/beta_{beta}.png")
            # plt.show()
            plt.close()

    if equalPeakReweigth:
        # Conversione ad array di numpy
        Lpr = np.array(Lpr)
        betapr = np.array(betapr)
        betaPpr = np.array(betaPpr)

        ### Media e deviazione standard a L fisso
        LprU = np.unique(Lpr)
        betaPprN = np.empty(len(LprU))
        betaPprS = np.empty(len(LprU))

        for i, L in enumerate(LprU):
            betaPprN[i] = np.average(betaPpr[Lpr == L])
            betaPprS[i] = np.std(betaPpr[Lpr == L], ddof=1)

        ### Grafici
        fig, ax = plt.subplots(1,1)

        # betaPpr in funzione di L, con linee a beta costante e barre d'errore
        if (ZoomStyle):
            errorStyle = {'marker':'x', 'markersize':5, 'capsize':3, 'elinewidth':0.5}
            plotStyle = {'marker':'.', 'markersize':3}
        else:
            errorStyle = {'markersize': 0, 'capsize':3, 'elinewidth':1.5}
            plotStyle = {'markersize': 0}
        ax.errorbar(LprU, betaPprN, yerr = betaPprS,
                    linestyle='', color='k', **errorStyle)
        ax.plot(Lpr, betaPpr, label=f"{beta}", linestyle='', color='r', **plotStyle)
        ax.set_xlabel(r"$L$")
        ax.set_ylabel(r"$\beta_{P}$")
        fig.savefig("./imgs/betaP_L.png")
        ax.clear()

        # betaCgauss in funzione di beta, con linee ad L costante
        for L in np.unique(Lgauss):
            maskPlt = Lgauss == L
            ax.plot(betagauss[maskPlt], betaCgauss[maskPlt], marker='.', label=f"{L}")
        ax.set_xlabel(r"$\beta$")
        ax.set_ylabel(r"$\beta_{P}$")
        plt.savefig("./imgs/betaP_beta.png")
        plt.clf()

    if printTable:
        # Questa cosa coi dizionari era pensata per aggirare la possibilità
        # che uno dei valori mancasse a causa del fallimento di fit/reweight
        tableDict = {}
        for L in np.unique(np.append(LgaussU, LprU)):
            tableDict[L] = {'gauss': "", 'pr': ""}
        for L, N, S in zip(LgaussU, betaCgaussN, betaCgaussS):
            tableDict[L]['gauss'] = f"{ufloat(N,S):.2uL}"
        for L, N, S in zip(LprU, betaPprN, betaPprS):
            tableDict[L]['pr'] = f"{ufloat(N,S):.2uL}"
        with open("./imgs/betaCtable.tex", 'w') as f:
            Lcycle = np.sort(np.unique(np.append(LgaussU, LprU)))
            for L in Lcycle:
                f.write(f"${L}$ & ${tableDict[L]['gauss']}$ & ${tableDict[L]['pr']}$ \\tabularnewline\n")