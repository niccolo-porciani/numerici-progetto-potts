#!/usr/bin/env python
# Argomenti da command line
#  - File prodotto da analysis.py

import sys
import numpy as np
import matplotlib.pyplot as plt
from uncertainties import ufloat_fromstr, correlated_values, unumpy
from scipy.optimize import curve_fit
import utils

plt.style.use("style.yaml")

converters = dict.fromkeys(
    [2, 3, 4],
    lambda col_bytes: ufloat_fromstr(col_bytes.decode("latin1"))
)
converters = {**{0: int, 1: float}, **converters}

obs = np.loadtxt(sys.argv[1], converters=converters, dtype=object)

Ls = np.unique(obs[:, 0])


# Collapse with expected values
beta_c = 0.550565
nu = 1./3.
alpha = 1.

figheight = 4.8
fig, ax = plt.subplots(1, 2, subplot_kw={"box_aspect": 1}, gridspec_kw={"wspace": .3}, figsize=(figheight * 2.3, figheight))

for L in Ls:
    if L < 22:
        continue
        # pass
    mask = obs[:, 0] == L
    obsL = obs[mask]

    beta = np.array(obsL[:, 1])
    idx = np.argsort(beta)
    beta = beta[idx]
    c_v = np.array(obsL[:, 4])[idx]

    x = (beta_c/beta - 1) * L**(1/nu)
    y = c_v / L**(alpha/nu)

    # Grafico senza errori perché altrimenti non si capisce
    ax[0].plot(
        beta, [v.n for v in c_v],
        linestyle='-', marker='.', label=f'{L}'
    )
    ax[1].plot(
        x, [v.n for v in y],
        linestyle='-', marker='.', label=f'{L}'
    )

ax[0].legend()
ax[1].legend()
ax[0].set_xlabel(r"$\beta$")
ax[1].set_xlabel(r"$(\beta_c/\beta - 1) \cdot L^{1/\nu}$")
ax[0].set_ylabel(r"$c_V$")
ax[1].set_ylabel(r"$c_V / L^{\alpha/\nu}$")
plt.savefig("./imgs/specific_heat_collapse.png")
plt.show()
plt.close()

# Fit maximun of the specific heat
c_v_max = np.empty(len(Ls), dtype=object)
for i, L in enumerate(Ls):
    mask = obs[:, 0] == L
    obsL = obs[mask]

    c_v_max[i] = np.max(obsL[:, 4])

# c_v_max = unumpy.uarray([_.n for _ in c_v_max], [_.s for _ in c_v_max])
# c_v_max_log = unumpy.log(c_v_max)
# L_log = np.log(Ls.astype(float))


def retta(x, m, alpha, q):
    return m*x**alpha + q


mask = Ls >= 22

xs = Ls**3
ys = unumpy.nominal_values(c_v_max)
yerr = unumpy.std_devs(c_v_max)
opt, cov = curve_fit(
    retta,
    xs[mask],
    ys[mask],
    sigma=yerr[mask],
    absolute_sigma=True,
    p0=(0.008, 1, 0)
)

chi2 = utils.chisq(xs[mask], ys[mask], yerr[mask], retta, opt)
ndof = len(ys[mask]) - len(opt)
opt = correlated_values(opt, cov)

print(f'A: {opt[0]:.2u}, B: {opt[2]:.2u}, esponente: {opt[1]:.2u}, chi2/ndof: {chi2 / ndof}')

plt.errorbar(
    xs[mask], ys[mask], yerr=yerr[mask],
    linestyle='', marker='.', color='blue'
)
plt.errorbar(
    xs[np.logical_not(mask)], ys[np.logical_not(mask)], yerr=yerr[np.logical_not(mask)],
    linestyle='', marker='.', color='gray'
)

delta = (xs.max() - xs.min())*0.05
x = np.linspace(max(xs.min() - delta, 0), xs.max() + delta)
plt.plot(x, retta(x, *unumpy.nominal_values(opt)), color='red')

plt.xlabel(r"$L^{3}$")
plt.ylabel(r"$\max c_{V}$")
plt.savefig("./imgs/fit_FSS_max_cv.png")

plt.show()
