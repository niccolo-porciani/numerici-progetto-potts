#!/usr/bin/env python
# Command line arguments:
#  - Path of folder with simulation data
#  - Block size
#  - File in which to save result

import sys
import numpy as np

from parallel_computation import (
    parallel_load_measurements,
    paralle_compute_observables
)

N_RESAMPLE = 100  # 1000
BLOCK_SIZE = int(sys.argv[2])

meas = parallel_load_measurements(sys.argv[1])
obs = paralle_compute_observables(meas, N_RESAMPLE, BLOCK_SIZE)

if len(sys.argv) == 4:
    np.savetxt(sys.argv[3], obs, fmt='%r')
