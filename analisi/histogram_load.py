#!/usr/bin/env python
# See python3 histogram_load.py -h for help

import numpy as np
import pickle
from parallel_computation import parallel_load_measurements
import argparse

parser = argparse.ArgumentParser(description='Load data and generate histogram data.')
parser.add_argument('dirpaths', metavar='dir', type=str, nargs='+',
                    help='Folders containing data files')
parser.add_argument('--out', '-o', type=str,
                    dest='outfile', action='store',
                    help='Destination file in which to save pickled data')
parser.add_argument('--nbins', type=int,
                    action='store', default=50,
                    help='Number of histogram bins')

if __name__ == "__main__":
    args = parser.parse_args()
    print(args)
    alldata = np.empty(0, dtype=[('L', int), ('beta', np.double), ('bins', object), ('hist', object)])
    for dirpath in args.dirpaths:
        print("Loading measurements...")
        meas = parallel_load_measurements(dirpath) # L, beta, E, M
        print("Done")

        data = np.empty(len(meas),
            dtype=[('L', int), ('beta', np.double), ('bins', object), ('hist', object)])

        for i, datapoint in enumerate(meas):
            L = datapoint[0]
            beta = datapoint[1]
            E_arr = datapoint[2]

            hist, binBounds = np.histogram(E_arr, bins=args.nbins)
            hist = hist / len(E_arr)
            bins = (binBounds[1:] + binBounds[:-1])/2

            print(f"i: {i}, L: {L}, beta: {beta}")

            data[i] = (L, beta, bins, hist)

        alldata = np.append(alldata, data)

    with open(args.outfile, 'wb') as f:
        pickle.dump(alldata, f)