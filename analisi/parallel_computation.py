import sys
import os
from cmath import rect
import numpy as np
import functools
import multiprocessing
from uncertainties import ufloat

import utils

np_rect = np.vectorize(rect)


def load_measurements(fname, measurements):
    E, N0, N1, N2 = np.loadtxt(fname, unpack=True, delimiter=',')
    params = utils.get_parameters(fname)

    beta = params['beta']
    L = params['L']

    E = E / L**3
    M = abs(np_rect(N0 / L**3, 0) + np_rect(N1 / L**3, 2*np.pi / 3) + np_rect(N2 / L**3, 4*np.pi / 3))

    measurements.append(np.array([L, beta, E, M], dtype=object))


def parallel_load_measurements(dirpath, func=load_measurements):
    manager = multiprocessing.Manager()
    meas_shared = manager.list()

    fnames = [os.path.join(dirpath, f) for f in os.listdir(dirpath)]

    pool = multiprocessing.Pool()
    pool.map(
        functools.partial(load_measurements, measurements=meas_shared),
        fnames
    )

    meas = np.empty(np.array(meas_shared).shape, dtype=object)
    np.copyto(meas, meas_shared)

    idx = np.lexsort((meas[:, 1], meas[:, 0]))
    meas = meas[idx]

    return meas


def compute_observables(measurement, N_RESAMPLE, BLOCK_SIZE, observables):
    L, beta, E, M = measurement

    u = E.mean()
    sigmaE_u = utils.mean_std(utils.blocking(E, BLOCK_SIZE))

    m = M.mean()
    sigmaE_m = utils.mean_std(utils.blocking(M, BLOCK_SIZE))

    c_v = L**3 * beta**2 * utils.std(E)**2
    sigmaE_c_v = utils.std(utils.bootstrap(
        E, lambda x: L**3 * beta * utils.std(x)**2,
        n_resample=N_RESAMPLE,
        block_size=BLOCK_SIZE
    ))

    u = ufloat(u, sigmaE_u)
    m = ufloat(m, sigmaE_m)
    c_v = ufloat(c_v, sigmaE_c_v)

    print(L, c_v)

    observables.append(np.array([L, beta, u, m, c_v]))


def paralle_compute_observables(measurements, N_RESAMPLE, BLOCK_SIZE):
    manager = multiprocessing.Manager()
    obs_shared = manager.list()

    pool = multiprocessing.Pool()
    pool.map(
        functools.partial(
            compute_observables,
            observables=obs_shared,
            N_RESAMPLE=N_RESAMPLE,
            BLOCK_SIZE=BLOCK_SIZE
        ),
        measurements
    )

    obs = np.empty(np.array(obs_shared).shape, dtype=object)
    np.copyto(obs, obs_shared)

    return obs
