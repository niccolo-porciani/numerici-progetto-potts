#!/usr/bin/env python

import numpy as np
import matplotlib.pyplot as plt
import matplotlib as mpl
import sys
import os
from cmath import rect
import argparse

import utils

np_rect = np.vectorize(rect)
plt.style.use("style.yaml")

parser = argparse.ArgumentParser(description='Load data and generate histogram data.')
parser.add_argument('fname', metavar='file', type=str, nargs=1,
                    help='Data file')
parser.add_argument('--savefig', type=str,
                    dest='outfile', action='store',
                    help='Destination file in which to save image')

args = parser.parse_args()
print(args)
fname = args.fname[0]
savefile = False
if hasattr(args, "outfile"):
    outfile = args.outfile
    savefile = True


E, N0, N1, N2 = np.loadtxt(fname, unpack=True, delimiter=',')
params = utils.get_parameters(fname)

L = params['L']
E = E / L**3
M = abs(np_rect(N0 / L**3, 0) + np_rect(N1 / L**3, 2*np.pi / 3) + np_rect(N2 / L**3, 4*np.pi / 3))

infostr = f"$L$: {L}, $\\beta$: {params['beta']}"

mpl.rcParams['agg.path.chunksize'] = 10000

plt.plot(range(len(E)), E, linewidth=0.2, color='black')
plt.title('Energia, ' + infostr)
plt.xlabel(r"$t_{MC} / T_\mathrm{sample}$")
plt.ylabel(r"$u$")
if savefile: plt.savefig(outfile + "_u.png")
plt.show()
plt.close()

plt.plot(range(len(M)), M, linewidth=0.2, color='black')
plt.title('Magnetizzazione, ' + infostr)
plt.xlabel(r"$t_{MC} / T_\mathrm{sample}$")
plt.ylabel(r"$m$")
if savefile: plt.savefig(outfile + "_m.png")
plt.show()
plt.close()
