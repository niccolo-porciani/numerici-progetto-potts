#!/usr/bin/env python
# Command line arguments:
#  - Data file produced by analysis.py

import sys
import numpy as np
from uncertainties import ufloat, ufloat_fromstr
import matplotlib.pyplot as plt

plt.style.use("style.yaml")

converters = dict.fromkeys(
    [2, 3, 4],
    lambda col_bytes: ufloat_fromstr(col_bytes.decode("latin1"))
)
converters = {**{0: int, 1: float}, **converters}

obs = np.loadtxt(sys.argv[1], converters=converters, dtype=object)

Ls = np.unique(obs[:,0])


def plot_obs(idx: int, title: str, xlabel: str, ylabel: str, save_name: str):
    for l in Ls:
        L = np.array(obs[:,0])
        mask = L == l
        beta = obs[mask,1]
        obs_ = obs[mask,idx]
        idx_sort = np.argsort(beta)
        beta = beta[idx_sort]
        obs_ = obs_[idx_sort]
        plt.errorbar(beta, [o.n for o in obs_], yerr=[o.s for o in obs_], label=f'{l}', linestyle='', elinewidth=1, marker='.', markersize=3)

    plt.title(title)
    plt.xlabel(xlabel)
    plt.ylabel(ylabel)
    plt.legend()
    # plt.show()
    plt.savefig(f"./imgs/{save_name}")
    plt.close()


plot_obs(2, 'Energy', r'$\beta$', r'$u$', 'energy.png')
plot_obs(3, 'Magnetization', r'$\beta$', r'$m$', 'magnetization.png')
plot_obs(4, 'Specific Heat', r'$\beta$', r'$c_V$', 'specific_heat.png')
