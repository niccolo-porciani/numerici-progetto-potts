\documentclass{article}

\usepackage[sectionClearpage=false, images=true]{npStyle}
\usepackage{catchfile}
\newcommand{\avg}[1]{\left\langle#1\right\rangle}
\newcommand{\red}[1]{\textcolor{red}{#1}}

\usepackage[
backend=biber
% ,style=authoryear
% ,dashed=false
% ,autocite=inline
% ,url=false
% ,doi=false
% ,eprint=false
% ,sorting=none
,style=numeric
]{biblatex}
\usepackage{csquotes}
\addbibresource{potts.bib}

\title{
    Modulo 1 \\
    \Large{Markov Chain Montecarlo per il modello di Potts $ d = 3 $ $ q = 3 $}
}
\author{Alessandro Piazza \and Porciani Niccolo'}

\begin{document}

\maketitle
\tableofcontents

\section{Modello di Potts}

Il modello di Potts è un modello statistico su reticolo \( \Lambda \) in cui ogni sito ha $ q $ stati o ``spin'' possibili, che denotiamo come $ 0,\dots,q-1 $. L'Hamiltoniana è data da
\[
    H = - J \sum_{\langle i, j \rangle} \delta_{s_i,s_j} - h \sum_i \delta_{s_i,\bar{s}}
\]
dove \( \langle i, j \rangle\) sono al solito i primi vicini e \( \bar{s} \in \{0, \ldots, q-1\} \) una direzione privilegiata (fissata) per l'accoppiamento con un campo esterno \( h \).
Nel resto della trattazione assumeremo senza perdita di generalità $ J = 1 $.\\
Abbiamo preso in considerazione il caso in cui il reticolo $ \Lambda $ è un reticolo cubico di lato \( L \) in tre dimensioni (volume \( V = L^{3} \)) con condizioni al bordo periodiche e $ q = 3 $, ovvero
\[
    s_i \in \{0,1,2\} \quad \forall i \in \Lambda
\]
Le osservabili rilevanti sono energia per unità di volume \( H / V \) e il parametro d'ordine, che per questo modello è il modulo della magnetizzazione complessa per unità di volume
\[
    m = \frac{1}{V}\sum_j e^{\frac{2\pi i}{3}s_j} = \sum_{k=0}^{2} \frac{N_k}{V} e^{\frac{2\pi i}{3}k}
\]
ove \( N_{i} \) è il numero di siti con spin \( s_{i} \). \\

Nel limite termodinamico il modello presenta una transizione di fase del primo ordine in $ h = 0 $. Al di sopra della temperatura critica \( T_{c} \) il sistema è in una fase disordinata, ci aspettiamo $ N_0 \simeq N_1 \simeq N_2 $ e quindi $ \abs{m} \simeq 0 $; al di sotto della temperatura critica avviene una rottura spontanea del gruppo di simmetria (\( S_{3} \)), viene selezionato uno degli stati del modello, \emph{wlog} \( N_{0} \simeq V, N_{1} \simeq N_{2} \simeq 0 \), e quindi si ha una fase ordinata con $ \abs{m} \simeq 1 $.

Essendo una transizione del primo ordine, l'energia media per unità di volume \( u = \braket{H} / V \) ha un comportamento discontinuo in \( \beta = \beta_{c} \) e quindi il calore specifico a volume costante
\[
    c_{V} = \dpd{u}{T} = -\beta^{2} \dpd{u}{\beta}
\]
avrà un comportamento ``a \( \delta \) di Dirac'' in tale punto. Al solito il calcolo del calore specifico può essere ricondotto alla varianza dell'energia
\[
    c_{V} = \frac{\beta^{2}}{V} \left(\braket{H^{2}} - \braket{H}^{2}\right) = \beta^{2} V \, \mathrm{Var}\left[\frac{H}{V}\right]
\]

Anche il parametro d'ordine è discontinuo in corrispondenza della temperatura critica, ma la ``suscettività magnetica'' è un parametro molto più complicato da trattare e non legabile direttamente alla varianza del parametro d'ordine. Infatti, essendo un modello a 3 stati potremmo aggiungere 3 accoppiamenti diversi con un campo esterno
\[
    H = - J \sum_{\langle i, j \rangle} \delta_{s_i,s_j} - h_{0} \sum_i \delta_{s_i,0} - h_{1} \sum_i \delta_{s_i,1} - h_{2} \sum_i \delta_{s_i,2}
\]
e definire quindi 3 magnetizzazioni \( m_{i} = N_{i}/V \) e 9 suscettività
\[
    \braket{m_{i}} = \frac{1}{V \beta} \dpd{\log{Z}}{h_{i}} \qquad \chi_{ij} = \dpd{\braket{m_{i}}}{h_{j}} = \beta V \left(\braket{m_{i} m_{j}} - \braket{m_{i}} \braket{m_{j}}\right) = \beta V\, \mathrm{Cov}(m_{i}, m_{j})
\]
La magnetizzazione complessa è quindi \( m = m_{0} + m_{1} e^{2\pi i /3} + m_{2} e^{4\pi i /3} \). \\

% \textcolor{red}{
% In presenza un \( h > 0 \), sono energeticamente favorite le configurazione con spin allineato lungo \( \bar{s} \) e quindi in generale si avrà \( m \simeq e^{2\pi \bar{s}/3} \). Per \( h < 0 \) lo stato \( \bar{s} \) è energeticamente sfavorito e, al di sotto di un valore critico del campo \( h_{c} < 0 \), il sistema diventa sostanzialmente equivalente ad un sistema a due stati ed è mappabile in un modello di Ising in \( d = 3 \); la transizione di fase diventa una transizione del secondo ordine. \\
% }

Nello studio della termodinamica del sistema tramite simulazioni Markov Chain Montecarlo abbiamo quindi preso \( h = 0 \) e ci siamo concentrati su
\begin{itemize}
    \item l'andamento qualitativo dell'energia media, modulo del parametro d'ordine e del calore specifico in funzione della temperatura
    \item effetti di volume finito sul calore specifico (\emph{finite size scaling})
    \item istogrammi dell'energia per una stima della temperatura critica con fit gaussiano e reweighting
\end{itemize}

\section{Effetti di volume finito}
In una transizione del primo ordine non si applica l'analisi di finite size scaling delle transizioni del secondo ordine in quanto la lunghezza di correlazione rimane finita e le grandezze termodinamiche non hanno un andamento universale a legge di potenza. Possiamo tuttavia determinare un tipo di scaling con il volume facendo dei modellini efficaci vicino alla temperatura critica.

% \subsection{Modello a 2 stati}

In un modello a 2 stati per il modello di Potts a \( q \) stati, la funzione di partizione è
\begin{equation}
    Z(\beta, V) = q e^{-\beta V f_{1}(\beta)} + e^{-\beta V f_{2}(\beta)}
\end{equation}
dove \( f_{1} \) e \( f_{2} \) sono rispettivamente le energie libere per unità di volume della fase ordinata e di quella disordinata. Poniamo
\begin{equation}
    p_{1} = \frac{e^{-\beta V f_{1}}}{Z} \qquad  p_{2} = \frac{e^{-\beta V f_{2}(\beta)}}{Z}
\end{equation}
La transizione si ha in corrispondenza del \( \beta \) per cui le probabilità relative \( q p\ped{o} \) e \( p\ped{d} \) sono uguali, ovvero nel limite termodinamico quanto \( f_{1}(\beta) = f_{2}(\beta) \). La discontinuità nell'energia per unità di volume tra le due fasi alla temperatura critica è quindi
\begin{equation}
    \Delta \epsilon = \epsilon_2(\beta_{c}) - \epsilon_1(\beta_{c}) = \left. \dpd{}{\beta}\left(\beta\left[f_{2}(\beta) - f_{1}(\beta)\right]\right)\right|_{\beta = \beta_{c}}
\end{equation}
da cui per \( \beta \simeq \beta_{c} \) si ha un sviluppo al primo ordine
\begin{equation}
    \beta(f_{2}(\beta) - f_{1}(\beta)) \simeq (\beta - \beta_{c}) \Delta \epsilon
\end{equation}
Per \( \beta \simeq \beta_{c} \) si può allora mostrare che il calore specifico per unità di volume è
\begin{equation}
    c_{V} = \beta^{2} V (\Delta \epsilon)^{2} \frac{e^{-V \Delta \epsilon (\beta - \beta_{c})}}{(1 + e^{-V \Delta \epsilon (\beta - \beta_{c})})^{2}}
\end{equation}
che è nella forma di una funzione di \( V (\beta - \beta_{c}) \) per un fattore di volume. Usando la notazione delle transizioni del secondo ordine si può allora scrivere uno scaling del tipo (\( V = L^{3} \))
\begin{equation}
    c_{V} = L^{\alpha / \nu} \phi(L^{1/\nu} t) \qquad \nu = 1/3, \alpha = 1
\end{equation}

In realtà ci aspettiamo una deviazione dall'andamento di pura legge di potenza per il massimo del calore specifico. Se facciamo un'espansione quadratica dell'energia libera vicino alla temperatura critica
\begin{equation}
    \beta f_{i}(\beta) \simeq \beta_{c} \bar{f} + a_{i} (\beta - \beta_{c}) + \frac{1}{2} b_{i}(\beta - \beta_{c})^{2}
\end{equation}
abbiamo che la funzione di partizione è
\begin{align}
  \log Z(\beta, V) &\simeq \log{(q+1)} -\beta_{c} \bar{f} V - \frac{V}{q+1}(q a_{1} + a_{2}) (\beta - \beta_{c}) \\
                   &\quad + \frac{1}{2} \frac{V}{q+1} \left[V \frac{q}{q+1} {(a_{1} - a_{2})}^{2} - (q b_{1} + b_{2})\right] (\beta - \beta_{c})^{2} \nonumber
\end{align}
L'energia interna è
\begin{equation}
    u = -\frac{1}{V} \dpd{\log Z(\beta, V)}{\beta} \simeq \frac{q a_{1} + a_{2}}{q+1} - \frac{1}{q+1}\left[V \frac{q}{q+1} {(a_{1} - a_{2})}^{2} - (q b_{1} + b_{2})\right] (\beta - \beta_{c})
\end{equation}
e il calore specifico è all'ordine zero
\begin{equation}
    c_{V} = -\beta^{2} \dpd{u}{\beta} \simeq \beta_{c}^{2} \frac{q}{(q+1)^{2}} (a_{1} - a_{2})^{2} V - \beta_{c}^{2}\frac{q b_{1} + b_{2}}{q+1}
\end{equation}
Pertanto per il massimo del calore specifico (che è la migliore stima che abbiamo del calore specifico alla ``temperatura critica'' su volume finito) ci possiamo aspettare una dipendenza dal volume della forma
\begin{equation}\label{eq:max_cv}
    \max c_{V}(\beta, L) = A L^{3} + B
\end{equation}
Vedremo in seguito che la correzione della costante è importante se si considerano \( L \sim 20-30 \).

\section{Stima della temperatura critica}

\subsection{Modello a 2 gaussiane}\label{sec:2gauss_model}
Usiamo un modello con densità di stati gaussiana per la fase ordinata e quella disordinata
\begin{equation}
    \Omega_{i}(E) = \frac{A_{i}}{\sqrt{2\pi} \sigma_{i}}e^{-\frac{(E - E_{i})^{2}}{2\sigma_{i}^{2}}}
\end{equation}
per cui la funzione di partizione totale è
\begin{equation}
    Z_{g}(\beta) = Z_{1}(\beta) + Z_{2}(\beta) = \int (\Omega_{1}(E) + \Omega_{2}(E)) e^{-\beta E} \dif{E} = A_{1} e^{-\beta(E_{1} - \sigma_{1}^{2}\beta / 2)} + A_{2} e^{-\beta(E_{2} - \sigma_{2}^{2}\beta / 2)}
\end{equation}
da cui vediamo che l'approssimazione gaussiana è equivalente ad una approssimazione lineare per le energie libere
\begin{equation}
    f_{i}(\beta) = \frac{E_{i}}{V} - \frac{\sigma_{i}^{2}\beta}{2V} = e_{i} - \frac{s_{i}^{2}}{2} \beta
\end{equation}
essendo \( E_{i} = V e_{i} \) e \( \sigma_{i} = \sqrt{V} s_{i} \). Se 1 e 2 sono rispettivamente la fase ordinata e quella disordinata si deve avere \( A_{1}/A_{2} = q \). Uguagliando le funzioni di partizione delle due gaussiane troviamo un'espressione per la temperatura critica
\begin{equation}
    \beta_{c} = \alpha \frac{1+\sqrt{1-4\gamma}}{2}
\end{equation}
essendo
\begin{equation}
    \alpha = 2 \frac{E_{1} - E_{2}}{\sigma_{1}^{2} - \sigma_{2}^{2}} = 2\frac{e_{1} - e_{2}}{s_{1}^{2} - s_{2}^{2}} \qquad
    \gamma = \frac{2}{\alpha^{2}} \frac{1}{\sigma_{1}^{2}-\sigma_{2}^{2}} \log\frac{A_{1}}{A_{2}} =  \frac{1}{V} \frac{2}{\alpha^{2}} \frac{1}{s_{1}^{2} - s_{2}^{2}} \log\frac{A_{1}}{A_{2}}
\end{equation}


La distribuzione dell'energia per unità di volume in approssimazione gaussiana è
\begin{equation}
    P_{g}(\beta, e) = \frac{V\Omega(Ve) e^{-\beta Ve}}{Z_{g}(\beta)} = \frac{Z_{1}(\beta)}{Z_{g}(\beta)} \frac{\sqrt{V}}{\sqrt{2\pi} s_{1}} e^{-V\frac{(e - e_{1} + s_{1}^{2} \beta)^{2}}{2s_{1}^{2}}} + \frac{Z_{2}(\beta)}{Z_{g}(\beta)} \frac{\sqrt{V}}{\sqrt{2\pi} s_{2}} e^{-V\frac{(e - e_{2} + s_{2}^{2} \beta)^{2}}{2s_{2}^{2}}}
\end{equation}
Di conseguenza, facendo l'istogramma delle energie ad una data temperatura tramite una simulazione Montecarlo e fittando tale istogramma con una somma di gaussiane
\begin{equation}
    P_{g}(\beta, e) = C_{1} e^{-\frac{(e - \mu_{1})^{2}}{2\xi_{1}^{2}}} + C_{2} e^{-\frac{(e - \mu_{2})^{2}}{2\xi_{2}^{2}}}
\end{equation}
possiamo ottenere delle stime per i parametri della densità di stati ed una stima della temperatura critica. Con qualche calcolo si trova
\begin{equation}
    e_i = \mu_i + \beta \xi_i^2 V
    \qquad
    s_i = \xi_i \sqrt{V}
\end{equation}
\begin{equation}
    \alpha = \frac{2}{V} \frac{\mu_{1} - \mu_{2}}{\xi_{1}^{2} - \xi_{2}^{2}} + 2 \beta \qquad
    \gamma = \frac{2}{\alpha^{2} V^2} \frac{1}{\xi_{1}^{2} - \xi_{2}^{2}}\left[\log\left(\frac{\xi_{1} C_{1}}{\xi_{2} C_{2}}\right) + \beta V (\mu_{1} - \mu_{2})\right] + \frac{\beta^2}{\alpha^{2}}
\end{equation}

\subsection{Reweighting}\label{sec:reweighting}

Un metodo alternativo \cite{JankeVillanova} per ottenere dei punti di pseudo-transizione che nel limite di \( L \to \infty \) tenderanno alla temperatura critica consiste nel considerare i \( \beta \) tali per cui i due picchi dell'istogramma hanno lo stesso valore nel massimo, che saranno indicati come \( \beta_P(L) \).

Per ottenere una stima di tali valori a partire da dati simulati a \( \beta \) generici abbiamo operato un ``reweighting'' dei dati. Tale procedura permette di ottenere una stima dell'istogramma ad un valore di \( \beta' \) diverso da quello simulato \( \beta \), come
\begin{equation}
    P(e,\beta') = \frac{Z(\beta)}{Z(\beta')} e^{-(\beta' - \beta)Ve}\, P(e,\beta)
\end{equation}
dove il rapporto delle funzioni di partizione è un fattore di normalizzazione irrilevante, che ignoreremo.

\section{Simulazioni}

Il modello è stato simulato su reticoli finiti di lato $ L $ al variare della temperatura inversa $ \beta $ utilizzando la tecnica del Markov Chain Monte Carlo con algoritmo Metropolis.

Il passo elementare della catena di Markov utilizzata consiste nello scegliere un sito casuale del reticolo e proporre una variazione dello stato del sito ad uno degli altri due stati possibili, scegliendo casualmente e accettando/rigettando la mossa secondo l'accettanza del Metropolis. Abbiamo considerato come ``passo'' della simulazione una ripetizione di $ L^3 $ passi elementari (in altri termini 1 tempo Montecarlo corrisponde a \( L^{3} \) update Metropolis) e denoteremo con $ N\ped{step} $ il numero di passi in una simulazione.

I primi $ N\ped{therm} $ passi di ogni simulazione sono stati scartati per permettere alla catena di Markov di ``termalizzare'', ovvero di convergere alla distribuzione invariante (ma si veda il seguito in merito). Le osservabili, ovvero energia e numeri aggregati di spin in ciascuno dei tre stati, sono stati misurati ogni $ T\ped{sample} $ passi.

\subsection{Implementazione}
L'algoritmo di Metropolis per il modello di Potts è stato implementato in \texttt{C++} ed è disponibile al repository \href{https://gitlab.com/niccolo-porciani/numerici-progetto-potts}{numerici-progetto-potts}, in particolare in \texttt{simulazioni/lattice.cpp}, dove sono anche disponibili gli script per l'analisi delle simulazioni e delle osservabili di interesse.

\subsection{Termalizzazione}
I dati ottenuti sono stati ispezionati visivamente tramite grafici di energia e parametro d'ordine in funzione del tempo Monte Carlo per accertare la corretta termalizzazione della catena.

Nella maggior parte dei casi il valore scelto di $ N\ped{therm} $ è risultato sufficiente, ma in alcuni casi a $ \beta > \beta_c $ (riportiamo un esempio in figura \ref{fig:freeze}) si è osservato un periodo iniziale in cui l'energia e il parametro d'ordine si attestavano ad un valore rispettivamente superiore e inferiore rispetto a quello atteso nella fase ordinata. A tale periodo iniziale seguiva una rapida transizione verso il regime asintotico atteso, ad un tempo apparentemente casuale.

In particolare il parametro d'ordine sembra stabilizzarsi (ma con fluttuazioni importanti) attorno a \( \abs{m} \simeq 1/2 \). Possiamo interpretare questa fase iniziale come una temporanea stabilizzazione della catena in una configurazione di coesistenza tra due fasi ordinate con diverso valore dello ``spin'', separate da uno o più bordi. In tale configurazione ci aspettiamo di avere \emph{wlog} \( N_{0} \simeq N_{1} \simeq L^{3}/2 \) e \( N_{2} \simeq 0 \) da cui
\begin{equation}
    \abs{m} \simeq \abs{\frac{1}{2} + \frac{1}{2} e^{2i \pi/3}} = \frac{1}{2}
\end{equation}
Ciò è compatibile con il fatto che al di sotto della temperatura critica la catena perde di ergodicità e tende a stabilizzarsi attorno ad uno dei vari minimi degeneri dell'energia. La situazione di coesistenza descritta rappresenta infatti un minimo locale ma non globale dell'energia, dove il sistema può stabilizzarsi temporaneamente prima di ``cadere'' nel vero minimo globale.

In ogni caso abbiamo deciso di ripetere le simulazioni che presentavano questo fenomeno, e non abbiamo incontrato ulteriori difficoltà.

\begin{figure}[h]
    \begin{center}
        \includegraphics[width=0.49\textwidth]{./imgs/freeze_u.png}
        \includegraphics[width=0.49\textwidth]{./imgs/freeze_m.png}
    \end{center}
    \caption{Energia per unità di volume e parametro d'ordine in funzione del numero di miusre (tempo Montecarlo fratto \( T\ped{sample} = 20 \)) per una simulazione che presenta il fenomeno descritto di ``congelamento''}
    \label{fig:freeze}
\end{figure}

\subsection{Dati raccolti}

Si sono eseguiti due gruppi di simulazioni, entrambi con \( N\ped{step} = 5 \times 10^6 \), \( T\ped{sample} = 20 \), \( N\ped{therm} = 3000 \), e

\begin{itemize}
    \item \texttt{dataset\_A}: \( L = \) 8, 12, 16 e da 20 a 42 ogni 2, 20 valori di \( \beta \) equispaziati in \( [0.549, 0.552] \)
    \item \texttt{dataset\_B}: \( L = \) da 34 a 42 ogni 2, 5 valori di \( \beta \) equispaziati in
    \begin{equation}
        \beta \in \beta_c\ap{att} + \sbr{- \frac{1}{L^3}, +\frac{1}{L^3}}
    \end{equation}
    (si veda in merito la sezione sul reweighting). Si è preso \( \beta_c\ap{att} = 0.550565 \), seguendo \cite{BonatiDElia}.
\end{itemize}

\section{Analisi}

\subsection{Osservabili e stima delle incertezze}

In Figura~\ref{fig:osservabili} riportiamo i grafici dell'energia e del parametro d'ordine per il \texttt{dataset\_A}.

Gli errori sono stati stimati tramite la tecnica del blocking, con taglia dei blocchi pari a $ 4000 $, scelta in corrispondenza del ``plateau'' per la simulazione più vicina alla temperatura critica attesa e con il massimo valore di $ L $, che ci aspettiamo avere tempo di correlazione più lungo. Il grafico rilevante è riportato in Figura~\ref{fig:blocking}. Per i dettagli implementativi si veda \texttt{analisi/analysis.py}.

\begin{figure}[h]
    \begin{center}
        \includegraphics[width=0.49\textwidth]{./imgs/energy.png}
        \includegraphics[width=0.49\textwidth]{./imgs/magnetization.png}
    \end{center}
    \caption{Energia per unità di volume e parametro d'ordine in funzione della temperatura, a diversi valori di $ L $ (in legenda) per il \texttt{dataset\_A}}
    \label{fig:osservabili}
\end{figure}

\begin{figure}[h]
    \begin{center}
        \includegraphics[width=0.48\textwidth]{./imgs/blocking_e.png}
        \includegraphics[width=0.48\textwidth]{./imgs/blocking_m.png}
    \end{center}
    \caption{Valori delle incertezze stimate per energia e magnetizzazione in funzione della taglia del blocco, per simulazioni a $ L = 42 $, $ \beta = 0.550567 $. La linea verticale coincide con la taglia effettivamente scelta per le analisi successive.}
    \label{fig:blocking}
\end{figure}

Dai dati si è poi calcolato il calore specifico per unità di volume $ c_V $, stimando l'errore tramite bootstrap con binning (essendo questo proporzionale alla varianza dell'energia) con la medesima taglia utilizzata precedentemente. Il grafico risultante è riportato in Figura~\ref{fig:specific_heat}.

\begin{figure}[h]
    \begin{center}
        \includegraphics[width=0.75\textwidth]{./imgs/specific_heat.png}
    \end{center}
    \caption{Calore specifico per unità di volume in funzione della temperatura, a diversi valori di $ L $ (in legenda) per il \texttt{dataset\_A}}
    \label{fig:specific_heat}
\end{figure}

Si osserva un accordo qualitativo con quanto atteso: l'energia e il parametro d'ordine hanno un andamento sempre più ripido vicino alla temperatura critica attesa (nel limite termodinamico ci aspettiamo una discontinuità), mentre il calore specifico ha un andamento a campana sempre vicino alla stessa, che si stringe e si alza all'aumentare di \( L \). Come atteso gli errori statistici sono più rilevanti vicino alla temperatura critica e a volume maggiore, a parità di tempo di simulazione Montecarlo.


\subsection{Finite Size Scaling}

Abbiamo innanzitutto studiato il collasso dei dati riportando $ c_V L^{\alpha / \nu} $ contro $ \del{\beta_c / \beta - 1} L^{1/\nu} $, utilizzando i valori noti in letteratura di
\[
    \beta_c\ap{att} = 0.550565 \qquad
    \nu = \frac{1}{3} \qquad
    \alpha = 1
\]
Tale grafico è riportato, raffrontato al normale grafico di $ c_V $ contro $ \beta $, in Figura~\ref{fig:collasso}. Come vedremo anche in seguito, le simulazioni fatte a \( 8 \leq L \leq 22 \) presentano una deviazione importante dallo scaling atteso, probabilmente conseguenza del fatto che essendo la transizione del primo ordine la lunghezza di correlazione non diverge e ci possono essere effetti sistematici fintanto che questa è comparabile con la taglia del sistema. Tali simulazioni sono quindi state escluse sia dal grafico del collasso sia dal fit successivo. Effetti sistematici persistono in realtà anche nel range \( 22 < L < 34 \), come si può osservare dal fatto che tali curve non collassano assieme alle altre, ma come vedremo si possono modellare tali effetti sistematici ai fini del fit.

\begin{figure}[h]
    \begin{center}
        \includegraphics[width=\textwidth]{./imgs/specific_heat_collapse.png}
    \end{center}
    \caption{Normale grafico $ c_V $ vs $ \beta $ (sinistra) e collasso dei dati (destra), \( L \geq 22 \). Le incertezze sono omesse per leggibilità. Si osserva effettivamente collasso per \( L \geq 34 \), mentre gli altri dati sono affetti da effetti sistematici.}
    \label{fig:collasso}
\end{figure}


Abbiamo poi studiato l'andamento del massimo del calore specifico in funzione del volume del sistema. Per volume grande ci aspettiamo un andamento a potenza \( \propto L^{3} \), deviazioni sistematiche per \( L \) ``piccoli'', ed eventualmente una regione intermedia in cui funzioni un andamento del tipo \( A L^{3} + B \) (si veda l'equazione~\ref{eq:max_cv}).


In Figura~\ref{fig:max_cv} è riportato il massimo di \( c_{V} \) in funzione del volume \( V = L^{3} \), che, come atteso, presenta deviazioni sistematiche dall'andamento lineare per \( L \lesssim 22 \). Si è poi eseguito un fit della forma \( f(x) = A x^{\alpha} + B \) per \( L \geq 22 \) ottenendo come parametri di best fit
\begin{equation}
    \alpha = 1.027 \pm 0.067 \qquad A = 0.0014 \pm 0.0011 \qquad B =12.6 \pm 2.8 \qquad \chi^{2}/\mathrm{ndof} = 0.114
\end{equation}
Il valore del \( \chi^2 \) ridotto risulta piuttosto basso, probabilmente a causa di una sovrastima delle incertezze nei dati ad \( L \) grande in quanto il tempo di simulazione è stato fissato in modo indipendente dal volume.

Osserviamo infine che la correzione data da \( B \) è abbastanza rilevante nel range di \( L \) esplorato. Possiamo dire che la costante diventa poco rilevante quando \( A L^{3 \alpha} \gtrsim 5 B \) ovvero per \( L \gtrsim 32 \). La stima dell' \( L \) minimo per cui inizia a valere la pura legge di potenza è quindi abbastanza in accordo con il collasso delle curve solo per \( L \geq 34 \). Il collasso e il fit sono implementati in \texttt{analisi/fss.py}.

\begin{figure}[h]
    \begin{center}
        \includegraphics[width=0.7\textwidth]{./imgs/fit_FSS_max_cv.png}
    \end{center}
    \caption{Fit lineare in scala bilogaritmica del massimo valore del calore specifico in funzione di $ L $}
    \label{fig:max_cv}
\end{figure}

\subsubsection{Istogrammi}

Dalle simulazioni eseguite abbiamo ricavato degli istogrammi della distribuzione dei valori dell'energia per unità di volume, che una volta normalizzati per il numero totale di valori risultano distribuiti secondo la probabilità
\begin{equation}
    P(e) = V P(Ve) = \frac{V\Omega(Ve) e^{-\beta V e}}{Z(\beta)}
\end{equation}
Qualitativamente la proprietà più rilevante di tali istogrammi è la presenza di due picchi distinti, caratteristica di una transizione di fase al primo ordine, che possiamo pensare come associati rispettivamente alla fase disordinata e alle due fasi ordinate in \( \Omega(E) \).
Ci aspettiamo inoltre che il peso relativo dei due picchi vari in funzione della temperatura a causa della presenza del fattore di Boltzmann, e che tale variazione sia tanto più repentina quanto più il volume è grande. Ciò è quanto si osserva in Figura~\ref{fig:hist_comparison}, in cui riportiamo gli istogrammi per diversi valori di \( \beta \) ad \( L = 32,42 \). Nel limite di volume infinito ci aspettiamo allora che la temperatura a cui i due picchi diventano uguali sia la temperatura critica associata alla transizione di fase.

\begin{figure}[h]
    \adjustbox{center}{
        \begin{tabular}{cccc}
            % Ho preso i due estrmi e i due accanto alla transizione
          \includegraphics[width=0.25\textwidth]{./imgs/histograms/L_32_beta_0.5504000.png} &
                                                                                              \includegraphics[width=0.25\textwidth]{./imgs/histograms/L_32_beta_0.5505330.png} &
                                                                                                                                                                                  \includegraphics[width=0.25\textwidth]{./imgs/histograms/L_32_beta_0.5505670.png} &
                                                                                                                                                                                                                                                                      \includegraphics[width=0.25\textwidth]{./imgs/histograms/L_32_beta_0.5507000.png} \\
          \includegraphics[width=0.25\textwidth]{./imgs/histograms/L_42_beta_0.5504000.png} &
                                                                                              \includegraphics[width=0.25\textwidth]{./imgs/histograms/L_42_beta_0.5505330.png} &
                                                                                                                                                                                  \includegraphics[width=0.25\textwidth]{./imgs/histograms/L_42_beta_0.5505670.png} &
                                                                                                                                                                                                                                                                      \includegraphics[width=0.25\textwidth]{./imgs/histograms/L_42_beta_0.5507000.png} \\
        \end{tabular}
    }
    \caption{Istogrammi dell'energia a diverse temperature (riportate in figura), per \( L = 32 \) (prima riga) ed \( L = 42 \) (seconda riga)}
    \label{fig:hist_comparison}
\end{figure}

% Commento generale: niente errori perché bootstrap sugli istogrammi, in ogni caso non si prevede dipendenza da beta quindi lo spread è stima dell'errore. Gnegne qualitativo eh.

In tutta l'analisi successiva abbiamo scelto di omettere le incertezze sugli istogrammi, che si sarebbero potute stimare tramite bootstrap, in quanto in entrambi i metodi di analisi ci aspettiamo che le quantità stimate siano approssimativamente indipendenti dal \( \beta \) di simulazione. Ci aspettiamo allora che a causa del tempo di simulazione limitato la principale fonte di incertezza sulle quantità stimate a un fisso \( L \) siano le fluttuazioni tra le simulazioni a diversi \( \beta \) e non le incertezze delle singole simulazioni. Nell'analisi successiva abbiamo allora preso come incertezza le suddette fluttuazioni.

Per i dettagli implementativi dell'analisi degli istogrammi si veda \texttt{analisi/histogram.py}.

\paragraph*{Fit gaussiano}

Seguendo il modello discusso in sezione~\ref{sec:2gauss_model} abbiamo deciso di fittare gli istogrammi con un modello a due gaussiane, della forma
\begin{equation}
    P_{g}(\beta, e) = C_{1} e^{-\frac{(e - \mu_{1})^{2}}{2\xi_{1}^{2}}} + C_{2} e^{-\frac{(e - \mu_{2})^{2}}{2\xi_{2}^{2}}}
\end{equation}
Per semplicità i fit sono in realtà stati eseguiti con un modello a singola gaussiana su due sottoinsiemi di dati, corrispondenti rispettivamente a \( e < -1.63 \) e \( e > -1.68 \).
Da tale fit possiamo ricavare delle stime per i parametri della densità di stati e per la temperatura critica, come descritto in sezione~\ref{sec:2gauss_model}.

Per osservare la dipendenza da \( L \) del valore così ottenuto di \( \beta_{c,g} \) abbiamo deciso di operare congiuntamente con entrambi i dataset, in quanto \texttt{dataset\_A} contiene un range più ampio di valori di \( L \), e limitandoci a utilizzare i dati per cui \( 0.5504 < \beta < 0.5507 \). I risultati, aggregati tra dati allo stesso \( L \), sono riportati in figura~\ref{fig:betaCgauss_L}, e quelli relativi al dataset \texttt{dataset\_B} sono anche riportati in tabella~\ref{tbl:betaC}.

Nei dati, particolarmente quelli a \( L \) piccolo, si osserva una marcata dipendenza da \( L \) stesso, qualitativamente compatibile con la presenza di correzioni FSS in potenze di \( 1/V \) rispetto al valore asintotico di \( \beta_c \). In prima analisi ci aspetteremmo quindi che i dati che meglio approssimano il valore asintotico siano quelli a \( L \) grande, come ad esempio quelli del \texttt{dataset\_B}.

Abbiamo poi deciso di eseguire un fit su questo comportamento FSS assumendo correzioni di ordine \( 1/V \) e prendendo quindi come modello
\begin{equation}
    \beta_{c,g}(L) = \beta_c\ap{FSS} + \frac{a}{L^3}
\end{equation}

La curva di best-fit è riportata in figura~\ref{fig:betaCgauss_L}, e i parametri di best-fit risultano essere

\begin{equation}
    \beta_{c}\ap{FSS} = 0.5505553 \pm 0.0000067 \qquad a = -5.110 \pm 0.047
\end{equation}

Osserviamo che il valore di \( \beta_c \) così ottenuto è compatibile con il valore noto in letteratura.

Per osservare viceversa un'eventuale dipendenza dal valore di \( \beta \) a cui veniva effettuata la simulazione abbiamo deciso di utilizzare solo il dataset \texttt{dataset\_B}, in quanto contenente più valori di \( \beta \) vicini alla transizione. I risultati sono riportati in figura~\ref{fig:betaCgauss_beta}.

% Commenti: Qualitativamente non si vede niente, ragionevole se il modello gaussiano \( \beta \)-indip è buono

Non si osserva alcuna evidente dipendenza di \( \beta_{c,g} \) da \( \beta \), quantomeno entro le fluttuazioni tra le diverse simulazioni, e ciò risulta compatibile con l'assunto che il modello a due gaussiane sia un'approssimazione ragionevole della vera densità di stati, almeno nel limite di grandi volumi in cui siano trascurabili effetti di superficie che scalano come \( L^2 \).

\begin{figure}[h]
    \centering
    \begin{subfigure}{.49\textwidth}
        \includegraphics[width=\textwidth]{./imgs/betaCgauss_L.png}
        \caption{Media e deviazione standard delle stime di \( \beta_{c,g} \) da dati a fisso \( L \), in funzione di \( L \), entrambi i dataset.}
        \label{fig:betaCgauss_L}
    \end{subfigure}\hspace{0.019\textwidth}%
    \begin{subfigure}{.49\textwidth}
        \includegraphics[width=\textwidth]{./imgs/betaCgauss_beta.png}
        \caption{Stime di \( \beta_c \) dai fit gaussiani in funzione di \( \beta \), dataset \texttt{dataset\_B}, con linee tra dati allo stesso \( L \)}
        \label{fig:betaCgauss_beta}
    \end{subfigure}
    \caption{Stime di \( \beta_c \) dai fit gaussiani}
\end{figure}

\paragraph*{Peak reweighting}

Abbiamo poi effttuato l'analisi di reweighting descritta in sezione~\ref{sec:reweighting}, operando per bisezione sui valori di \( \beta' \) fino a trovare un \( \beta' \) per cui valesse
\begin{equation}
    \abs{\ln P_+ - \ln P_-} < 0.01
\end{equation}
dove \( P_\pm \) sono i valori di \( P(E) \) ai due picchi e si è scelto di operare con \( \ln P \) per comodità di implementazione.
Numericamente la procedura presenta problemi se \( \abs{(\beta' - \beta)Ve} \gg 1 \) a causa della precisione finita dei numeri in virgola mobile. Per alleviare tale problema abbiamo deciso di includere parzialmente il fattore di normalizzazione, operando il reweighting come
\begin{equation}
    P(e,\beta') = e^{+(\beta' - \beta) \avg{e}}\, e^{-(\beta' - \beta)Ve}\, P(e,\beta) = e^{-(\beta' - \beta)V(e - \avg{e})}\, P(e,\beta)
\end{equation}
tale scelta è vantaggiosa in quanto i valori di \( e \) variano approssimativamente nell'intervallo \( [-1.9,-1.5] \), e si ha quindi che \( \abs{e - \avg{e}} \sim 0.2 \), guadagnando un fattore 10 sul range di \( \beta' - \beta \) permessi. Abbiamo scelto in ogni caso di limitarci a utilizzare dati per cui l'esponente di reweighting avesse modulo inferiore a 8, inizializzando la bisezione su un intervallo in \( \beta \) di larghezza pari a \( 8 / (V \sigma_e) \). Queste problematiche hanno motivato la scelta di un range di temperatura variabile nel \texttt{dataset\_B}.

Analogamente a quanto fatto per \( \beta_{c,g} \) abbiamo osservato la dipendenza da \( L \) in entrambi i dataset (figura~\ref{fig:betaP_L} e tabella~\ref{tbl:betaC} per i soli dati in \texttt{dataset\_B}) e la dipendenza da \( \beta \) nel dataset \texttt{dataset\_B} (figura~\ref{fig:betaP_beta}).

% Commenti: La dipendenza da L è meno chiara e a L piccolo fa schifo perché pochi dati, a L grosso però funziona bene, non ci sono dipendenze da beta

Il comportamento FSS di \( \beta_P \) risulta meno chiaro rispetto a quello di \( \beta_{c,g} \), ma è comunque qualitativamente compatibile con la presenza di un valore asintotico con correzioni a volume finito. Rispetto a \( \beta_{c,g} \) i valori di \( \beta_P \) ad \( L \) grandi sono però in accordo molto migliore con il valore noto di \( \beta_c \), avendo incertezza comparabile. Anche in questo caso non sono discernibili dipendenze da \( \beta \) entro le fluttuazioni.

\begin{figure}[h]
    \centering
    \begin{subfigure}{.49\textwidth}
        \includegraphics[width=\textwidth]{./imgs/betaP_L.png}
        \caption{Dati, media e deviazione standard delle stime di \( \beta_{P} \) da dati a fisso \( L \), in funzione di \( L \), entrambi i dataset.}
        \label{fig:betaP_L}
    \end{subfigure}\hspace{0.019\textwidth}%
    \begin{subfigure}{.49\textwidth}
        \includegraphics[width=\textwidth]{./imgs/betaP_beta.png}
        \caption{Stime di \( \beta_P \) in funzione di \( \beta \), \texttt{dataset\_B}, con linee tra dati allo stesso \( L \)}
        \label{fig:betaP_beta}
    \end{subfigure}
    \caption{Stime di \( \beta_c \) dai fit gaussiani}
\end{figure}

\CatchFileDef{\betaCtblcontents}{./imgs/betaCtable.tex}{}
\begin{table}
    \centering
    \begin{tabular}{lcc}
      \toprule
      \( L \) & \( \beta_{c,g} \) & \( \beta_P \)\\
      \midrule
      \betaCtblcontents
      \bottomrule
    \end{tabular}
    \caption{Stime di \( \beta_{c,g} \) e \( \beta_P \) aggregate tra dati a fisso \( L \) nel dataset \texttt{dataset\_B}}
    \label{tbl:betaC}
\end{table}

\printbibliography{}

\end{document}

% Metodologia -> split in simulazione e analisi