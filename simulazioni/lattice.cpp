#include "lattice.hpp"

// Librerie incluse nell' header
#include <memory>   // unique_ptr
#include <vector>

// Altre librerie
#include <iostream>
#include <fstream>
#include <random>
#include <string>
#include <cmath>

// pcg
#include "pcg-cpp/include/pcg_random.hpp"

lattice::lattice(unsigned int L_, initMode mode, double J_, double h_) {
    // Costruttore standard con la complicazione di make_unique
    L = L_;
    L3 = L * L * L;
    J = J_;
    h = h_;
    sites = std::make_unique<spin[]>(L3); // Questa cosa sembra inizializzare a 0
    switch (mode) {
    case allRandom: {
        // Generatore casuale
        pcg_extras::seed_seq_from<std::random_device> seed_source;
        pcg32 gen(seed_source);
        std::uniform_int_distribution<> randSpin(0, 2);

        for (unsigned int k = 0; k < L3; k++) {
            sites[k] = static_cast<spin>(randSpin(gen));
        }
        break;
    }
    case allZero:
        break;
    }

    nn_sites = new unsigned int * [L3];
    for (unsigned int i = 0; i < L3; i++) {
        std::vector<int> v = idx_to_coord(i);
        int x = v[0], y = v[1], z = v[2];
        nn_sites[i] = new unsigned int[NN_NUMBER]{
                coord_to_idx(x + 1, y, z),
                coord_to_idx(x, y + 1, z),
                coord_to_idx(x, y, z + 1),
                coord_to_idx(x - 1, y, z),
                coord_to_idx(x, y - 1, z),
                coord_to_idx(x, y, z - 1)
                };
    }
}

lattice::~lattice()
{
    for (unsigned int i = 0; i < L3; i++) {
        delete[] nn_sites[i];
    }

    delete [] nn_sites;
}

inline unsigned int modulo(int a, int b)
{
    return ((a % b) + b) % b;
}

std::vector<int> lattice::idx_to_coord(unsigned int i) const
{
    return {static_cast<int>(i / (L * L)), static_cast<int>((i % (L * L)) / L), static_cast<int>((i % (L * L)) % L)};
}

unsigned int lattice::coord_to_idx(int x, int y, int z) const
{
    return modulo(x,L) * L * L + modulo(y,L) * L + modulo(z,L);
}

inline spin lattice::idx(int x, int y, int z) const
{
    // Come accessore potrei fare tre cose
    // 1. Overloadare [], ma ho il problema che poi mi trovo con un array 2D
    // 2. Una member function idx(x,y,z) che restituisca il puntatore giusto
    // 3. Una member function idx(x,y,z) che restituisca direttamente il valore
    // La 3 è più comoda, perché se sei fuori dalla struct è più comodo L.idx() che L[L.idx()],
    // ma la 2 può essere messa come inline, che forse è più veloce. Per ora metto la 3
    return sites[modulo(x,L) * L * L + modulo(y,L) * L + modulo(z,L)];
}

void lattice::print_slices() const
{
    for (unsigned int k = 0; k < L3; k++) {
        std::cout << sites[k] << " ";
        if ((k+1) % L == 0)
            std::cout << "\n";
        if ((k+1) % (L * L) == 0)
            std::cout << "\n";
    }
}

double lattice::energy() const
{
    double E = 0.;

    for (unsigned int i = 0; i < L3; i++) {
        spin s = sites[i];
        for (unsigned int nn = 0; nn < NN_NUMBER / 2; nn++) {
            E += (s == sites[nn_sites[i][nn]]) ? -J : 0;
        }
        E += (s == H_DIRECTION) ? h : 0;
    }

    return E;
}

std::vector<int> lattice::magnetization() const
{
    std::vector<int> M = {0,0,0};

    for (unsigned int i = 0; i < L3; i++) {
        M[sites[i]] += 1;
    }

    return M;
}

// double lattice::DeltaH(unsigned int i, spin s) const
// {
//     spin s0 = sites[i];

//     double DeltaE = 0.;
//     for (unsigned int nn = 0; nn < NN_NUMBER; nn++) {
//         DeltaE += ((s == sites[nn_sites[i][nn]]) ? -J : 0)
//                 - ((s0 == sites[nn_sites[i][nn]]) ? -J : 0);
//     }

//     DeltaE += (s == H_DIRECTION ? h : 0) - (s0 == H_DIRECTION ? h : 0);

//     return DeltaE;
// }

void lattice::montecarlo(double beta, int N_step, int sample_freq, int sample_start, const std::string& path)
{
    // Generatore casuale
    pcg_extras::seed_seq_from<std::random_device> seed_source;
    pcg32 gen(seed_source);

    // Distribuzioni necessarie
    std::uniform_int_distribution<> rand_site(0, L3 - 1);
    std::uniform_int_distribution<> rand_spin(1, 2);
    std::uniform_real_distribution<double> rand_acc(0,1);

    int N_sample = (N_step - sample_start) / sample_freq
                  + ((N_step - sample_start) % sample_freq != 0 ? 1 : 0);

    double * energies = new double[N_sample];
    std::vector<int>* mags = new std::vector<int>[N_sample];

    // Precompute acceptance ratio
    double acc[4*NN_NUMBER + 1][3];
    for (int k = 0; k < 4*NN_NUMBER + 1; k++) {
        int Delta_Links = k - 2*NN_NUMBER;
        for (int j = 0; j < 3; j++) {
            int Delta_h = j - 1;
            acc[k][j] = exp(-beta * (-J * Delta_Links + h * Delta_h));
        }
    }


    unsigned int curr_idx = 0;

    for (int t = 0; t < N_step; t++) {
        for (unsigned int i = 0; i < L3; i++){
            unsigned int i_rand = rand_site(gen);
            spin s0 = sites[i_rand];
            spin s = static_cast<spin>((s0 + rand_spin(gen)) % 3);
            // double acc = exp(- beta * DeltaH(i_rand, s));

            unsigned int Delta_links_idx = 2*NN_NUMBER, Delta_h_idx = 1;
            for (unsigned int nn = 0; nn < NN_NUMBER; nn++) {
                spin s_nn = sites[nn_sites[i_rand][nn]];
                if (s == s_nn) {
                    Delta_links_idx++;
                }
                if (s0 == s_nn) {
                    Delta_links_idx--;
                }
            }
            if (s == H_DIRECTION)
                Delta_h_idx++;
            if (s0 == H_DIRECTION)
                Delta_h_idx--;

            double acc_ = acc[Delta_links_idx][Delta_h_idx];

            if ((acc_ > 1) || (rand_acc(gen) < acc_)) {
                sites[i_rand] = s;
            }
        }

        // Misure
        if (t >= sample_start && (t - sample_start) % sample_freq == 0) {
            energies[curr_idx] = energy();
            mags[curr_idx] = magnetization();
            curr_idx++;
        }
    }

    // Stampa a file
    std::ofstream file;
    file.open(path + "/potts_"
              + std::to_string(L) + "_"
              + std::to_string(beta) + "_"
              + std::to_string(J) + "_"
              + std::to_string(h) + ".csv");
    file << "# L: " << L << ", "
           << "beta: " << beta << ", "
           << "J: " << J << ", "
           << "h: " << h << ", "
           << "N_step: " << N_step << ", "
           << "sample_start: " << sample_start << ", "
           << "sample_freq: " << sample_freq << "\n";
    for (int i = 0; i < N_sample; i++) {
        file << energies[i] << ", "
             << mags[i][0] << ", "
             << mags[i][1] << ", "
             << mags[i][2] << "\n";
    }
    file.close();
}
