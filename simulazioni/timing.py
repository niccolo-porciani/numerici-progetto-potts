#!/usr/bin/env python

import matplotlib.pyplot as plt
import numpy as np
import datetime


def parsetime(v):
    return datetime.datetime.strptime(v.decode('ascii'), r'%d-%H:%M')


t, L = np.loadtxt('timing.txt', unpack=True, dtype=object, converters={0: parsetime, 1: int})
t = np.array([(_ - t[0]).total_seconds() / 60 for _ in t])

x = []
size_L = 31
for k in range(int(len(L) / size_L)):
    x.append([L[k * size_L], t[k * size_L:(k + 1) * size_L]])

thread = 11
for i in range(len(x)):
    y = x[i][1]
    x[i][1] = [y[k*thread:(k+1)*thread].mean() for k in range(int(len(y) / thread) + 1)]

x = np.array([[x[i][0], x[i][1][j]] for i in range(len(x)) for j in range(len(x[i][1]))])
print(x)
L, t = x.transpose()

t = t[1:] - t[:-1]

print(t)
plt.plot(L[1:], t, linestyle='', marker='x')
plt.xlabel('L')
plt.ylabel('average time [minutes]')
plt.show()
