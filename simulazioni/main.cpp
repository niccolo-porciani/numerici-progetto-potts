#include <iostream>
#include <string>
#include <yaml-cpp/yaml.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <omp.h>
#include <cmath>

#include "lattice.hpp"

#define BETA_C 0.550565
#define DELTA_E 0.5


std::vector<int> int_linspace(const int start, const int stop, const int step)
{
    std::vector<int> x;
    const int size = 1 + (stop - start) / step;
    x.push_back(start);
    for (int i = 1; i < size; i++) {
        x.push_back(x[i-1] + step);
    }

    return x;
}

std::vector<double> linspace(const double start, const double stop, const int size)
{
    std::vector<double> x;
    double delta = (stop - start) / (double) (size - 1);

    for (int i = 0; i < size; i++) {
        x.push_back(start + i * delta);
    }

    return x;
}

std::vector<double> inverse_volume_linspace (const int L, const int size)
{
    return linspace(BETA_C - 1./(2 * pow(L, 3) * DELTA_E), BETA_C + 1./(2 * pow(L, 3) * DELTA_E), size);
}


int main(int, char * argv[]) {
    // Load configuration parameters
    YAML::Node config = YAML::LoadFile(argv[1]);

    const int cache_size = config["cache_size"].as<int>();
    const int max_num_threads = config["max_num_threads"].as<int>();
    const std::string out_dir = config["out_dir"].as<std::string>();

    // Lattice parameters
    const double J = config["lattice"]["J"].as<double>();
    const double h = config["lattice"]["h"].as<double>();
    const int L_start = config["lattice"]["L_start"].as<int>(),
        L_stop = config["lattice"]["L_stop"].as<int>(),
        L_step = config["lattice"]["L_step"].as<int>();
    const int N_beta = config["lattice"]["N_beta"].as<int>();
    const double beta_start = config["lattice"]["beta_start"].as<double>(),
        beta_stop = config["lattice"]["beta_stop"].as<double>();

    // Simulation parameters
    const int N_step = config["simulation"]["N_step"].as<int>(),
        sample_freq = config["simulation"]["sample_freq"].as<int>(),
        sample_start = config["simulation"]["sample_start"].as<int>();

    // Create output directory
    mkdir(out_dir.c_str(), 0755);

    std::vector<int> L = int_linspace(L_start, L_stop, L_step);
    std::vector<double> beta = linspace(beta_start, beta_stop, N_beta);

    // Simulations
    for (unsigned i = 0; i < L.size(); i++){

        const int num_threads = std::min(max_num_threads, cache_size / (36 * (int) pow(L[i], 3)));
        std::cout << "Using " << num_threads << " threads\n";
        // omp_set_num_threads(num_threads);

// #pragma omp parallel for schedule(dynamic, 1)
        for (unsigned j = 0; j < beta.size(); j++) {
            std::cout << "L: " << L[i] << "  beta: " << beta[j] << "\n";

            lattice latt(L[i], allRandom, J, h);
            latt.montecarlo(beta[j], N_step, sample_freq, sample_start, out_dir);
        }
    }

    return 0;
}
