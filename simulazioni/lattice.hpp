#pragma once

#include <memory>   // unique_ptr
#include <vector>

#define NN_NUMBER 6
#define H_DIRECTION 2

enum spin {
    r = 0,
    g = 1,
    b = 2
};

enum initMode {
    allZero = 0,
    allRandom = 1
};

struct lattice {
    unsigned int L, L3;
    std::unique_ptr<spin[]> sites;
    unsigned int ** nn_sites;
    double J;
    double h;

    lattice(unsigned int L_, initMode mode, double J_, double h_);
    ~lattice();

    // Accessore in PBC
    inline spin idx(int x, int y, int z) const;

    std::vector<int> idx_to_coord(unsigned int i) const;
    unsigned int coord_to_idx(int x, int y, int z) const;

    // Stampa
    void print_slices() const;

    // Funzioni per il montecarlo
    double energy() const;
    std::vector<int> magnetization() const;

    // double DeltaH(unsigned int i, spin s) const;

    // Simulazione
    void montecarlo(double beta, int N_step, int sample_freq, int sample_start, const std::string& path);
};
